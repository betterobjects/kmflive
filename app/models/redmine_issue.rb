class RedmineIssue < ActiveRecord::Base
  
  include Live
  
  establish_connection Rails.configuration.database_configuration["redmine"]
  
  self.table_name = 'issues'
  
  belongs_to :author, :foreign_key => :author_id, :class_name => "RedmineUser"
  belongs_to :assigned_to, :class_name => "RedmineUser"
  belongs_to :tracker, :class_name => "RedmineTracker"
  
  #
  #
  def created_at
    self.created_on - 2.hours
  end
  
  #
  #
  def self.latest( n = 10 )
    self.order( created_on: :desc ).limit( n )
  end
  
end