class RedmineUser < ActiveRecord::Base
    
  establish_connection Rails.configuration.database_configuration["redmine"]
  
  self.table_name = 'users'
  self.inheritance_column = nil
  
  #
  #
  def name
    "#{firstname} #{lastname}"
  end
    
  #
  #
  def created_at
    self.created_on
  end
  
end