class OhiwaProjectfamily < ActiveRecord::Base
  include Live
  
  establish_connection Rails.configuration.database_configuration["ohiwa"]
 
  self.table_name = 'projectfamilies'
  
  belongs_to :client, class_name: "OhiwaClient"
  
end
