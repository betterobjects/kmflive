class OhiwaProject < ActiveRecord::Base
  include Live
  
  establish_connection Rails.configuration.database_configuration["ohiwa"]
 
  self.table_name = 'projects'
  
  belongs_to :projectfamily, class_name: "OhiwaProjectfamily"
  
  def client
    self.projectfamily.client
  end
  
end
