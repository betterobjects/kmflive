class OhiwaTimebooking < ActiveRecord::Base
  include Live
  
 establish_connection Rails.configuration.database_configuration["ohiwa"]
 
 self.table_name = 'timebookings'

  belongs_to :user, foreign_key: 'employee_id', :class_name => "OhiwaUser" 
  belongs_to :project, :class_name => "OhiwaProject" 
  
  
  #
  #
  def self.latest( n = 10 )
    self.order( created_at: :desc ).limit( n )
  end
  
end
