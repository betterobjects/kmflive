module Live
  
  
  #
  #
  def <=> other
    self.created_at <=> other.created_at
  end
  
  #
  #
  def css_id
    "#{self.class.to_s}-#{self.id}"
  end
end