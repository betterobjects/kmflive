class OhiwaSecretAccount < ActiveRecord::Base
    
  establish_connection Rails.configuration.database_configuration["ohiwa"]
  
  self.table_name = 'secret_accounts'
  self.inheritance_column = nil
    
end