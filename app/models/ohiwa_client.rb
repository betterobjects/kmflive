class OhiwaClient < ActiveRecord::Base
  include Live
  
 establish_connection Rails.configuration.database_configuration["ohiwa"]
 
 self.table_name = 'clients'
 
end
