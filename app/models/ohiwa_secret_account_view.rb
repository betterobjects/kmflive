class OhiwaSecretAccountView < ActiveRecord::Base
  include Live
  
  establish_connection Rails.configuration.database_configuration["ohiwa"]
  
  self.table_name = 'secret_account_views'
  self.inheritance_column = nil
  
  belongs_to :user, foreign_key: 'employee_id', :class_name => "OhiwaUser" 
  belongs_to :account, foreign_key: 'secret_account_id', :class_name => "OhiwaSecretAccount" 
  
  #
  #
  def self.latest( n = 5 )
    self.order( created_at: :desc ).limit( n )
  end
  
end