class OhiwaUser < ActiveRecord::Base
    
  establish_connection Rails.configuration.database_configuration["ohiwa"]
  
  self.table_name = 'employees'
  self.inheritance_column = nil
  
  #
  #
  def name
    "#{firstname} #{lastname}"
  end
  
  #
  #
  def admin?
    self.status = 'admin' 
  end   
end