class RedmineTracker < ActiveRecord::Base
    
  establish_connection Rails.configuration.database_configuration["redmine"]
  
  self.table_name = 'trackers'
  self.inheritance_column = nil
  
  
end