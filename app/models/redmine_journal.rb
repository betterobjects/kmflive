class RedmineJournal < ActiveRecord::Base
  
  include Live
  
  establish_connection Rails.configuration.database_configuration["redmine"]
  
  self.table_name = 'journals'
  
  belongs_to :user, :class_name => "RedmineUser"
  belongs_to :issue, :foreign_key => :journalized_id, :class_name => "RedmineIssue"
  belongs_to :tracker, :class_name => "RedmineTracker"
  
  #
  #
  def created_at
    self.created_on - 2.hours
  end
  
  #
  #
  def self.latest( n = 10 )
    self.where(journalized_type: 'Issue').order( created_on: :desc ).limit( n )
  end
  
  
end
