module ApplicationHelper
  
  #
  #
  def lna( date )
    date.blank? ? '' :  l( date, format: "%d.%m. %H:%M" )
  end

  #
  #
  def min2time( minutes )
    return "" unless minutes
    minus = false
    if minutes < 0 
      minus = true
      minutes = minutes.abs
    end
    days = minutes / 1440 
    res = ""
    if days > 0 
      minutes = minutes % 1440
      res = "#{days}T "
    end
    res << l((Time.mktime(0) + minutes.minutes), :format => "%H:%M")
    
    res = "- #{res}" if minus
    res
  end


end

