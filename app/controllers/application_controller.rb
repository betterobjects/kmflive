class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :sso
  
  protected 
  
  #
  #
  def sso
    return if session[:user]

    encrypted = cookies[:sso]
    if encrypted 
      cipher = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
      cipher.decrypt
      cipher.key = Digest::SHA1.hexdigest("kmf-ohiwa-crm-2013-83jdhHU7Grt34")
      decrypted = cipher.update(encrypted)
      decrypted << cipher.final
    
      user = OhiwaUser.where( email: decrypted ).first
    
      if user and user.admin?
        puts "\n\n\n----------------signed in: #{user.name}-----------------------\n\n\n"
        session[:user] = user.id
        redirect_to root_path
        return
      end
    end
    
    redirect_to Rails.configuration.sso_sign_in_url
  end
  
end
