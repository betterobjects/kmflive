class LiveController < ApplicationController
  
  #
  #
  def index
    
    if request.xhr?
      
      list = []
    
      list += RedmineIssue.latest( 10 )
      list += OhiwaTimebooking.latest( 10 )
      list += RedmineJournal.latest( 10 )
      list += OhiwaSecretAccountView.latest( 5 )
    
      @list = list.sort
      
      render action: 'ticker', layout: false
    end
    
  end
end
